<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Budget;

/**
 * @Route("/budget", name="budget")
 */

class BudgetController extends Controller
{
    private $serializer;
    
    public function __construct(SerializerInterface $serializer) {
            $this->serializer = $serializer;
    }
    /**
     * @Route("/", methods="GET")
     */
    public function findAll() {
        $repo = $this->getDoctrine()->getRepository(Budget::class);
        $budgets = $repo->findAll();
        $json = $this->serializer->serialize($budgets, "json");
    
            return JsonResponse::fromJsonString($json);
    }

    /**
     * @Route("/{id}", methods="GET")
     */
    public function findOne(Budget $budget) {
        
        $json = $this->serializer->serialize($budget, "json");

        return JsonResponse::fromJsonString($json);
    }
    
     /**
     * @Route("/{id}", methods="DELETE")
     */
    public function delete(Budget $budget) {
        
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($budget);
        $manager->flush();

        return new JsonResponse([], 204);
    }

    /**
     * @Route("/", methods="POST")
     */
    public function add(Request $req) {
        $manager = $this->getDoctrine()->getManager();

        $budget = $this->serializer->deserialize($req->getContent(),
                                                    Budget::class, 
                                                    "json");
        $manager->persist($budget);
        $manager->flush();

        $json = $this->serializer->serialize($budget, "json");

        return JsonResponse::fromJsonString($json, 201);
    }

    /**
     * @Route("/{id}", methods="PUT")
     */
    public function update(Budget $budget, Request $req) {
        $manager = $this->getDoctrine()->getManager();

        $body = $this->serializer->deserialize($req->getContent(),
                                                    Budget::class, 
                                                    "json");
        $budget->setContent($body->getContent());                                                    
        $budget->setDate($body->getDate());                                                    
        $budget->setSender($body->getSender());

        $manager->flush();

        $json = $this->serializer->serialize($budget, "json");

        return JsonResponse::fromJsonString($json);
    }

}
