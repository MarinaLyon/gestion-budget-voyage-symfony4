<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Expense;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/expense", name="expense")
 */
class ExpenseController extends Controller
{
    private $serializer;
    
    public function __construct(SerializerInterface $serializer) {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/", methods="GET")
     */
    public function findAll() {
        $repo = $this->getDoctrine()->getRepository(Expense::class);
        $expenses = $repo->findAll();
        $json = $this->serializer->serialize($expenses, "json");

        return JsonResponse::fromJsonString($json);
    }

    /**
     * @Route("/{id}", methods="GET")
     */
    public function findOne(Expense $expense) {
        
        $json = $this->serializer->serialize($expense, "json");

        return JsonResponse::fromJsonString($json);
    }

    /**
     * @Route("/{id}", methods="DELETE")
     */
    public function delete(Expense $expense) {
        
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($expense);
        $manager->flush();

        return new JsonResponse([], 204);
    }

    /**
     * @Route("/", methods="POST")
     */
    public function add(Request $req) {
        $manager = $this->getDoctrine()->getManager();

        $expense = $this->serializer->deserialize($req->getContent(),
                                                    Expense::class, 
                                                    "json");
        $manager->persist($expense);
        $manager->flush();

        $json = $this->serializer->serialize($expense, "json");

        return JsonResponse::fromJsonString($json, 201);
    }
    /**
     * @Route("/{id}", methods="PUT")
     */
    public function update(Expense $expense, Request $req) {
        $manager = $this->getDoctrine()->getManager();

        $body = $this->serializer->deserialize($req->getContent(),
                                                    Expense::class, 
                                                    "json");
        $expense->setContent($body->getContent());                                                    
        $expense->setDate($body->getDate());                                                    
        $expense->setSender($body->getSender());

        $manager->flush();

        $json = $this->serializer->serialize($expense, "json");

        return JsonResponse::fromJsonString($json);
    }
}
